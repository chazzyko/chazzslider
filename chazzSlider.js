	

(function(){
	
	var url = 'img/';
	
	var imgObjArr = [
		{'url':"samsung_galaxy_s5_neo.jpg" , 'alt':"Samsung Galaxy S5 Neo", 'desc': 'Samsung Galaxy S5 Neo', 'price': '200 Eur', 'link': 'http://allegro.pl/nowy-samsung-galaxy-s5-neo-czarny16gb-wysylka-free-i5965012162.html'},
		{'url':"apple-watch.png" , 'alt':"apple watch"},
		{'url': "glasses.jpg" , 'alt':"Prada glasses"},
		{'url': "jimmy-choo.jpg" , 'alt':"Jimmy Choo shoes"},
		{'url': "e-bike.jpg" , 'alt':"e-bike" },
		//s{'url':"lego.jpg" , 'alt':"lego"},
		{'url':"ipad3.jpg" , 'alt':"ipad"},
		{'url':"swarovski.jpg" , 'alt':"Swarowski crystals"},
		{'url':"bag.jpg" , 'alt':"D&G bags"},
		{'url':"kiteboard.jpg" , 'alt':"Kite board"},
		{'url':"hugoboss.jpg" , 'alt':"Hugo Boss Fragrance"},
		{'url':"dress.png" , 'alt':"Dress"},
		{'url':"gopro.jpg" , 'alt':"Go Pro Camera"},
		{'url':"nikeshoes.jpg", 'alt': 'Nike sneakers'},
		{'url':"Converse.jpg", 'alt': 'Converse sneakers'},
		{'url':"laptop.jpg", 'alt': 'Samsung laptop'},
		 {'url':"womenboots.jpg", 'alt': 'Ladies boots'},
		{'url':"armanijeans.jpg", 'alt': 'Armani jeans'},
		{'url':"flattv.jpg", 'alt': 'Samsung TV'},
		{'url':"chanel.jpg", 'alt': 'Channel fragrance'},
		{'url':"nikon2.jpg", 'alt': 'Nikon camera'},
		{'url':"dg.jpg" , 'alt':"Dolce Gabanna"},
		{'url':"rmouse.jpg" , 'alt':"Razor mouse"},
		// {'url':"man_boots.jpg" , 'alt':"Mans boots"},
		// {'url':"redbike.jpg" , 'alt':"E-becycle"},
		// {'url':"dronetoy.jpg" , 'alt':"Drone"},
		// {'url':"adidastop.jpg" , 'alt':"Adidas tracksuit top"},
		// {'url':"winter.jpg" , 'alt':"Snowboard"},
		// {'url':"razer.jpg" , 'alt':"Razer"}
	];
	
	var chazSlider = {
		self: false,
		url : '/img',
		count : 0,
		arrLength : imgObjArr.length - 1,
		imgClass : '',
		imgObj : {},
		imgParent : {},
		clickToggle : 0,
		setInt : false,
		setTimeOut : false,
		currentImg : '',
		allImgs : '',
		loadedCount : 0,
		loadFromHtml : false,
		
		init : function(){
			self = this;
			this.imgObj = imgObjArr[this.count];
			this.imgParent = $('#chazzSlider');
						
			this.imgParent.on('click', this.parentClick.bind(this));
			this.setInt = this.startSetInt(self);
		},
						
		startSetInt : function(self){
			return setInterval(function(){
					self.ajaxSlider();
			}, 3500, true);
		},		
		
		ajaxSlider : function(){
			
			if(this.count <= this.arrLength){
				
				this.imgClass = '';
				if( this.imgObj.desc != undefined ){
					this.imgClass = 'hoverThis';
				}
				var img = '<img class="chazzImg img-responsive animated fadeIn '+ this.imgClass +'" src="'+ url + this.imgObj.url +'" data-currentcount="'+ this.count +'" alt="' + this.imgObj.alt+'">';
				
				this.imgParent.find('img').addClass('hide');				
				this.imgParent.append(img);				
				
				this.currentImg = this.count;
				if(this.count <= this.arrLength){
					this.count++;					
					this.loadFromHtml = true;
				}			
			}else{
				this.fromHtml();				
			}			
			
			this.imgObj = imgObjArr[this.count];
		},
		
		fromHtml : function(){
			if(this.loadFromHtml){					
				this.allImgs = this.imgParent.find('.chazzImg');
				this.loadFromHtml = false;
			}
			
			for(var i = 0; i < this.allImgs.length; i++){
					this.allImgs[i].className = 'hide';
				}
								
			this.allImgs[this.loadedCount].className = 'chazzImg img-responsive animated fadeIn';
			this.currentImg = this.loadedCount;
			
			if(this.loadedCount < this.arrLength){
				this.loadedCount++;
			}else{
				this.loadedCount = 0;
			}
		},
		
		parentClick : function(){
			clearTimeout(this.setTimeOut);
			if(this.clickToggle == 0){
				clearInterval(this.setInt);
				this.clickToggle = 1;
				var currImg = imgObjArr[this.currentImg];
				if(currImg.desc != undefined){
					var hoverContent = '<div id="hoverText"><p>' + currImg.desc + '</p>' + 
										'<p class="animated flipInY">' + currImg.price + '</p>' + 
										'<p><a href="' + currImg.link + '" target="_blank"> >> '+ currImg.desc +'</a></p></div>';
					this.imgParent.append(hoverContent);
				}
				
				this.setTimeOut = setTimeout(function(){
					self.restartSlider();
				}, 5000);
				
			}else{
				this.restartSlider();
			}		
		
		},
		
		restartSlider : function(){
			this.clickToggle = 0;
			this.setInt = this.startSetInt(self);			
			this.imgParent.find('#hoverText').remove();
		}
	};
		
	chazSlider.init();
})();